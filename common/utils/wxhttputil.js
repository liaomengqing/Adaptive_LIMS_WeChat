var app = getApp();
var host = 'http://localhost:8080/renren-fast/';

var token = wx.getStorageSync('token');
token = (typeof token == "undefined" || token == null || token == "") === true ? '' : token;
/**
 * POST请求，
 * URL：接口
 * pram：参数，json类型
 * doSuccess：成功的回调函数
 * doFail：失败的回调函数
 */
var postDate = function post(url, pram, doSuccess, doFail) {
  wx.request({
    //项目的真正接口，通过字符串拼接方式实现
    url: host + url,
    header: {
      "content-type": "application/json;charset=UTF-8",
      "token": token.token
    },
    data: pram,
    method: 'POST',
    success: function (res) {
      doSuccess(res);
    },
    fail: function (res) {
      doFail(res);
    },
  })
}

//GET请求，不需传参，直接URL调用，
var getDate = function get(url, pram, doSuccess, doFail) {
  wx.request({
    url: host + url,
    header: {
      "content-type": "application/json;charset=UTF-8",
      "token": token
    },
    data: pram,
    method: 'GET',
    success: function (res) {
      doSuccess(res);
    },
    fail: function (res) {
      doFail(res);
    },
  })
}

module.exports = {
  postDate: postDate,
  getDate: getDate,
  host: host
}