Page({
  data:{
    mine_lab:[
      {
        id: 0,
        name: '个人信息',
      },
      {
        id: 1,
        name: '轮值表'
      },
      {
        id: 2,
        name: '位置安排'
      }
    ],
    page_name: '个人中心',
    mine_image: '/image/head_portralt/zhuo_zhuo_qi_hua.png',
    mine_name: '灼灼其华',
    project_number: '3',
    friends_number: '12',
    setting_icon: '/image/system_icon/setting_icon.png',
    ability_percent: '22',
    currentData: 0
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },
  //获取当前滑块的index
  bindchange: function (e) {
    const that = this;
    that.setData({
      currentData: e.detail.current
    })
  },
  //点击切换，滑块index赋值
  checkCurrent: function (e) {
    const that = this;

    if (that.data.currentData === e.target.dataset.current) {
      return false;
    } else {

      that.setData({
        currentData: e.target.dataset.current
      })
    }
  }
})