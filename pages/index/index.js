const wxhttputil = require('../../common/utils/wxhttputil.js');
const app = getApp()
Page({
  data:{
    app_name: '实验室自适应信息管理平台',
    imgUrls: [
      {
        link: './banner/banner',
        url: '/image/index/banner/2.jpg',
        title: '梧州学院软件开发中心招新啦'
      },
      {
        link: './banner/banner',
        url: '/image/index/banner/banner2_img.png',
        title: '来来来，这样买黄金更省钱'
      },
      {
        link: './banner/banner',
        url: '/image/index/banner/banner3_img.png',
        title: '七夕遇到前任，你最想对他说什么'
      },
      {
        link: './banner/banner',
        url: '/image/index/banner/banner4_img.png',
        title: '肯德基超值三件套来袭，只要22元'
      },
    ],
    labImg: [
      {
        id: 1,
        link: './banner/banner',
        url: '/image/index/lab_overview/pyramid.png',
        info: '实验室简介'
      },
      {
        id: 2,
        link: './banner/banner',
        url: '/image/index/lab_overview/regulation.png',
        info: '实验室守则'
      },
      {
        id: 3,
        link: './banner/banner',
        url: '/image/index/lab_overview/faculty.png',
        info: '师资队伍'
      },
      {
        id: 4,
        link: './banner/banner',
        url: '/image/index/lab_overview/stu_team.png',
        info: '学生团队'
      }
    ],
    labProject: [
      {
        id: 1,
        link: './banner/banner',
        headline: '环境监测系统',
        summary: '环保治理，监测先行，监测工作为环保治理提供数据支撑，环境监测系统的建设起到了重要的科学依据。',
        url: '/image/index/lab_project/environmental_img.png'
      },
      {
        id: 2,
        link: './banner/banner',
        headline: '元件管理系统',
        summary: '随着元件使用范围不断增广，粗放式管理的缺陷日益暴露，电子元件得不到有效的管理是开放实验室存在的突出的问题，许多贵重的电子元件没有得到有效地充分的利用，造成了极大的浪费。',
        url: '/image/index/lab_project/element_img.png'
      }
    ],
    indicatorDots: true,
    autoplay: true,
    circular: true,
    interval: 5000,
    duration: 1000,
    Height: '',
    //搜索框
    searchstr: '',
    //实验室概况
    left_rectangle: '/image/index/lab_overview/left_rectangle.png'
  },
  //获取最新热点新闻
  banner: function(e) {
    
  },
  imgHeight: function (e) {
    var winWid = wx.getSystemInfoSync().windowWidth; //获取当前屏幕的宽度
    var imgh = e.detail.height;//图片高度
    var imgw = e.detail.width;//图片宽度
    //等比设置swiper的高度。 即 屏幕宽度 / swiper高度 = 图片宽度 / 图片高度  ==》swiper高度 = 屏幕宽度 * 图片高度 / 图片宽度
    var swiperH = winWid * imgh / imgw + "px"
    this.setData({
      Height: swiperH//设置高度
    })
  },
  summaryWidth: function(e){
    var winWid = wx.getSystemInfoSync().windowWidth; //获取当前屏幕的宽度
    var summaryW = winWid - 450 + "px"
    this.setData({
      Width: summaryW//设置宽度
    })
  },
  changeIndicatorDots: function (e) {
    this.setData({
      indicatorDots: !this.data.indicatorDots
    })
  },
  changeAutoplay: function (e) {
    this.setData({
      autoplay: !this.data.autoplay
    })
  },
  intervalChange: function (e) {
    this.setData({
      interval: e.detail.value
    })
  },
  durationChange: function (e) {
    this.setData({
      duration: e.detail.value
    })
  },
  //搜索框输入时触发
  searchList(ev) {
    let e = ev.detail;
    this.setData({
      searchstr: e.detail.value
    })
  },
  //搜索回调
  endsearchList(e) {
    console.log('查询数据')
  },
  // 取消搜索
  cancelsearch() {
    this.setData({
      searchstr: ''
    })
  },
  //清空搜索框
  activity_clear(e) {
    this.setData({
      searchstr: ''
    })
  },
  blue_rectangle:function(e){
    const ctx = wx.createCanvasContext('myCanvas');
    ctx.rect(10, 10, 150, 75);
    ctx.setFillStyle('red');
    ctx.fill();
    ctx.draw();
  },
  onLoad: function (options) {
    var that = this;
    that.setImgUrls();
  },
  setImgUrls: function () {
    console.log("初始化轮播图")
    var token = wx.getStorageSync("token")
    console.log("token=" + token)
    wxhttputil.postDate('business/info/swiper', null, this.imgUrlSuccess, this.fail)
    console.log("初始化轮播图完成")
  },
  //接收后台回调函数
  imgUrlSuccess: function (data) {
    console.log("data.code=" + data.code)
    if (data && data.code === 0 && data != undefined ) {
      console.log("data.article=" + data.article)
      this.imgUrls = data.article;
    }else {
      this.imgUrls = []
    }
  },
  fail: function (res) {
  },
})