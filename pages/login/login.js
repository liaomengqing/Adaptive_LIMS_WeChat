const wxhttputil = require('../../common/utils/wxhttputil.js');
const app = getApp()
Page({
  getCaptchaUrl: function () {
    this.data.uuid = Math.round(Math.random() * 10000000000);
    this.data.captchaUrl = wxhttputil.host + 'captcha.jpg?uuid=' + this.data.uuid;
    this.setData({ uuid: this.data.uuid });
    this.setData({ captchaUrl: this.data.captchaUrl });
    this.setData({ captchaValue: '' });
  },
  onLoad: function (options) {
    var that = this;
    that.getCaptchaUrl();
  },
  data:{
    /**
     * 定义初始化变量
     */
    mobile : null,
    password : null,
    uuid: '',
    captchaUrl: '',
    captchaValue: '',
    mobileValue: '',
    passwordValue: '',
    mobile_icon: '/image/system_icon/account_icon.png',
    password_icon: '/image/system_icon/password_icon.png',
    verification_code: '/image/system_icon/verification_code.png',
    captchaInput: '请输入验证码',
    mobileInput: '请输入用户名',
    passwordInput: '请输入密码',
    login_btn: '登陆'
  },
  /**
   * 获取用户名并赋值给初始化的用户名变量
   */
  mobileInput: function(event){
    this.setData({ mobileValue : event.detail.value })
  },
  /**
   * 获取密码并复制给初始化的密码变量
   */
  passwordInput: function(event){
    this.setData({ passwordValue : event.detail.value })
  },
  /**
   * 获取密码并复制给初始化的密码变量
   */
  captchaInput: function (event) {
    this.setData({ captchaValue: event.detail.value })
  },
  /**
   * 监听
   */
  loginBtnClick: function () {
    var pram = {
      "username": this.data.mobileValue,
      "password": this.data.passwordValue,
      "captcha": this.data.captchaValue,
      "uuid": this.data.uuid
    };
    wxhttputil.postDate('sys/login', pram,  this.success, this.fail);
  },
    //接收后台回调函数
  success: function (res) {
    console.log(res)
    if (res.data.token !== '' && res.data.token !== null && res.data.token !== undefined) {
      wx.setStorageSync("token", res.data.token);
    }
    var msg = res.data.msg;
    if (msg == "success") {
      //调用微信弹窗组件提示成功
      wx.showToast({
        title: '登陆成功',
        duration: 2000   //弹窗延迟时间
      });
      wx.switchTab({
        url: '../index/index',
      })
    } else {
      //调用微信弹窗组件提示失败
      wx.showToast({
        title: '登陆失败',
        icon: 'none',
        duration: 2000  //弹窗延迟时间,
      });
      this.getCaptchaUrl();
    }
  },
  fail: function (res) {
  }
})