Page({
  data: {
    topic_image: [
      {
        id: 1,
        link: '',
        title: 'ISA/TMG系列',
        summary: '49377人参与'
      },
      {
        id: 2,
        link: '',
        title: '企业级安全系列',
        summary: '49300人参与'
      },
      {
        id: 3,
        link: '',
        title: 'JUNIPER系列',
        summary: '11688人参与'
      }
    ],
    color: [
      {
        id: 1,
        color: '#9999FF'
      },
      {
        id: 2,
        color: '#66CC66'
      },
      {
        id: 3,
        color: '#FFDF25'
      },
      {
        id: 4,
        color: '#ff99f7'
      },
      {
        id: 0,
        color: '#ff999e'
      }
    ],
    growth_record:[
      {
        id: 1,
        nickname: '不染',
        time: '刚刚发布',
        avatar: '/image/head_portralt/bu_ran.png',
        text: '分布式、高并发、多线程，到底有什么区别？',
        picture: [
          
        ],
        like: 2,
        comment: [

        ],
        link: ''
      },
      {
        id: 2,
        nickname: '放逐回忆',
        time: '15分钟前',
        avatar: '/image/head_portralt/fang_zhu_hui_yi.png',
        text: '你想要的JAVA资料这里都有!!!\n1.资源标题：程序员的SQL金典（完整）\n资源地址：http://down.51cto.com/data/2207566 \n2.资源标题：尚硅谷--2015年最新SpringMvc视频教程（Spring4版本）全60集\n资源地址：http://down.51cto.com/data/2064331',
        picture:[
          {
            url: '/image/circle/growing_image/001.png'
          }
        ],
        like: 100,
        comment:[
          {
            text: '感谢大佬的分享，适合IT学习路上各阶段的小伙伴！'
          },
          {
            text: '送花送花，同城技术交流群请进：33568459831'
          },
          {
            text: '求spring源码，有木有可以分享出来的小可爱'
          }
        ],
        link: ''
      }
    ],
    page_name: '圈子',
    //话题推荐
    topic_text: '话题推荐',
    topic_logo: '/image/circle/icon/recommended_topic_icon.png',
    topic_search: '/image/system_icon/search_icon.png',
    //创建话题
    create_topic: '/image/circle/icon/create_topic_icon.png',
    creat_topic_text: '来说点什么吧~',
    creat_topic_url: '',
    //成长“爪”迹
    growing: '成长爪迹',
    growing_image: '/image/circle/icon/growing_up_icon.png',
    like_normal: '/image/system_icon/like_normal.png',
    like_selected: '/image/system_icon/like_selected.png',
    comment_normal: '/image/system_icon/comment_normal.png',
    comment_selected: '/image/system_icon/comment_selected.png'
  }
})